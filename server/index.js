import express from 'express'
import {graphqlHTTP} from "express-graphql";
import cors from 'cors'
import schema from './schema.js'
const app = express()
app.use(cors({

}))

const users = [{
    id:1,
    username: 'Vasya',
    age: 25
}]
const createUser = input => {
    const id = Date.now()
    return {
        id, ...input
    }
}
const root = {
    getAllUsers: () => users,
    getUser: (id) => users.fint(user => user.id === id),
    createUser: ({input}) => {
        const user = createUser(input)
        users.push(user)
        return user
    }
}


app.use('/graphql', graphqlHTTP({
    graphiql: true,
    schema,
    rootValue: root
}))


app.listen(5000, () => console.log('server started on port 5000'))